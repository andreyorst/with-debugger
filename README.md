# with-debugger.fnl

A (work in progress) expression-level step debugger for Fennel.

# Requirements

`with-debugger` currenly works with fennel's main (unreleased) branch.

# Usage

Import macros provided in the library and wrap a form with `with-debugger`:

```fennel
(import-macros {: with-debugger} :with-debugger)

(with-debugger
  (let [a (if (= 0 (% (math.random 100) 2))
              27
              42)
        b (accumulate [res 0
                       _ v (ipairs [1 2 3])]
            (+ res v))]
    (+ a b)))
```

Upon execution, a debugger prompt will be presented:

```
Enterred step debugger. Commands:
c: continue, s/n: step over, p: pretty print value, q: exit
4:14:42 (= 0 (% (math.random 100) 2)) => false
debugger>> n
9:12:20 (+ res v) => 1
debugger>> n
9:12:20 (+ res v) => 3
debugger>> n
9:12:20 (+ res v) => 6
debugger>> n
10:4:10 (+ a b) => 48
```

Each message uses the format `line:start_col:end_col expression => result`.

Nested debuggers are supported.
